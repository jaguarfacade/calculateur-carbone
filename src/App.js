import logo from './logo.svg';
import React, { useState, useEffect } from 'react';
import './App.css';
import TextField from './TextField';
import IngredientsArea from './IngredientsArea';
import { accentInsensitiveRegex } from './Utils'
import ingredients_database from './Agribalyse_base_donnees.json';
import ciqual_database from './Table_CIQUAL.json';

import * as XLSX from 'xlsx';
import ExcelJS from 'exceljs';



function App() {
  // Récupération base de données
  const [ingredients, setIngredients] = useState(ingredients_database);

  // Définition
  const [recipeName, setRecipeName] = useState('');
  const [numberOfPortions, setNumberOfPortions] = useState('');
  const [searchQuery, setSearchQuery] = useState([]);
  const [selectedIngredients, setSelectedIngredients] = useState([]);
  const [searchBars, setSearchBars] = useState([]);
  const [filteredIngredients, setFilteredIngredients] = useState([]);
  const [selectedIngredientQuantities, setSelectedIngredientQuantities] = useState([]);
  const [selectedIngredientsEnergy, setSelectedIngredientsEnergy] = useState([]);

  // Ajout fonction étapes
  const [steps, setSteps] = useState([]);

  const addStep = () => {
    setSteps([...steps, ""]); // Ajoute une nouvelle étape vide
  };

  const handleStepChange = (e, index) => {
    const newSteps = [...steps];
    newSteps[index] = e.target.value;
    setSteps(newSteps);
  };
  // Fin ajout fonction étapes

  // Ajout fonction export
  const [fileBuffer, setFileBuffer] = useState(null);
  useEffect(() => {
    fetch(process.env.PUBLIC_URL + '/Template_recette.xlsx', {
      headers: {
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      },
    })
      .then((response) => response.arrayBuffer())
      .then((data) => setFileBuffer(data));
  }, []);

  const exportToExcel = async () => {
    // Charger le workbook à partir du buffer
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.load(fileBuffer);

    // Accéder à la première feuille de calcul
    const worksheet = workbook.getWorksheet(1);

    // Écrire dans les cellules
    worksheet.getCell('B4').value = recipeName || "Sans nom";
    worksheet.getCell('C6').value = numberOfPortions || 0;
    worksheet.getCell('C8').value = totalCarbon.toFixed(0) || 0;

    const startRow = 11;
    const endRow = 25;

    for(let i = 0; i < Math.min(endRow - startRow + 1, selectedIngredients.length); i++) {
      const currentRow = startRow + i;
      
      worksheet.getCell(`B${currentRow}`).value = selectedIngredients[i] ? selectedIngredients[i]['Nom du Produit en Français'] : '';
      
      const carbonFootprintCell = worksheet.getCell(`C${currentRow}`);
      carbonFootprintCell.value = selectedIngredients[i] ? selectedIngredients[i]['Total'] : 0;
      
      const quantityCell = worksheet.getCell(`D${currentRow}`);
      quantityCell.value = Number(selectedIngredientQuantities[i]) || 0;
      quantityCell.numFmt = '0';

      const energyCell = worksheet.getCell(`H${currentRow}`);
      energyCell.value = Number(selectedIngredientsEnergy[i]) || 0;
      energyCell.numFmt = '0';

      const energyPortionCell = worksheet.getCell(`I${currentRow}`);
      energyPortionCell.value = Number(selectedIngredientsEnergy[i] / 100 * selectedIngredientQuantities[i] / numberOfPortions) || 0;
      energyPortionCell.numFmt = '0';
    };

    for(let i = 0; i < Math.min(10, steps.length); i++) {
      worksheet.getCell(`B${i + 29}`).value = steps[i] ? steps[i] : '';
      worksheet.getCell(`B${i + 29}`).alignment = { wrapText: true };
    };

    // Convertir le workbook en buffer
    const buffer = await workbook.xlsx.writeBuffer();
          
    // Enregistrement
    const blob = new Blob([buffer], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
    const link = document.createElement("a");
    link.href = URL.createObjectURL(blob);

    const fileName = recipeName ? `${recipeName}.xlsx` : "Sans nom.xlsx";
    link.download = fileName;
    
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };
  
  // Fin ajout fonction export  
  // Ajouter fonction import
  const fileInputRef = React.useRef();

  const handleImportExcel = (event) => {
    // Tu traiteras le fichier choisi ici
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        const buffer = e.target.result;
        const workbook = XLSX.read(buffer, { type: 'buffer' });
        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];

        const recipeCell = worksheet['B4']; // B4 est la cellule contenant le titre de la recette
        if (recipeCell) {
          const recipeTitle = recipeCell.v; // 'v' est la valeur de la cellule
          setRecipeName(recipeTitle);
        }

        const portionCell = worksheet['C6'];
        if (portionCell) {
          const numberOfPortions = portionCell.v; // 'v' est la valeur de la cellule
          setNumberOfPortions(numberOfPortions);
        }

        let newIngredients = [];
        let quantities = [];
        for (let i = 11; i <= 25; i++) {
          const cellAddressIngredients = `B${i}`;
          const cellAddressQuantities = `D${i}`;

          const IngredientCell = worksheet[cellAddressIngredients];
          const quantitiesCell = worksheet[cellAddressQuantities];

          if (IngredientCell && IngredientCell.v) {
            const ingredientName = IngredientCell.v;
            newIngredients.push(ingredientName);
          } else {
            break; // Sortir de la boucle si la cellule est vide
          }

          if (quantitiesCell && quantitiesCell.v) {
            selectedIngredientQuantities.push(quantitiesCell.v);
          } else {
            break; // Sortir de la boucle si la cellule est vide
          }

        }

        setSearchBars(newIngredients.map((_, index) => ({})));
        setSearchQuery(newIngredients);
        const newSelectedIngredients = newIngredients.map(ingredientName => {
          return ingredients.find(ing => ing['Nom du Produit en Français'] === ingredientName) || {};
        });
        setSelectedIngredients(newSelectedIngredients);
        setSelectedIngredientQuantities(selectedIngredientQuantities);



      };
      reader.readAsArrayBuffer(file);
    }
  };

  const handleImportClick = () => {
    fileInputRef.current.click(); // Déclenche le clic sur le champ de saisie de fichier
  };
  // Fin ajout fonction import

  const handleSearchQuery = (query, index) => {
    const newQuery = [...searchQuery];
    if (typeof newQuery[index] === 'undefined') {
      newQuery[index] = '';
    }
    newQuery[index] = (query);
    setSearchQuery(newQuery);
  
    const regexQuery = accentInsensitiveRegex(newQuery[index]);
    const newFilteredIngredients = [...filteredIngredients];
    const filtered = ingredients.filter(ingredient => 
      regexQuery.test(ingredient['Nom du Produit en Français'].toLowerCase())
    );
    newFilteredIngredients[index] = filtered;
    setFilteredIngredients(newFilteredIngredients);
  };
  
  

  const handleSelection = (ingredientName, index) => {
    const ingredientObject = ingredients.find(ing => (ing['Nom du Produit en Français']) === (ingredientName[index]));
    
    if(ingredientObject) {
      const newSelectedIngredients = [...selectedIngredients];
    newSelectedIngredients[index] = ingredientObject;
    setSelectedIngredients(newSelectedIngredients);

    const codeCIQUAL = ingredientObject["Code CIQUAL"];
    const CIQUALItem = ciqual_database.find(item => item["Code CIQUAL"] === codeCIQUAL);

    if(CIQUALItem){
      const energy = [...selectedIngredientsEnergy];
      energy[index] = CIQUALItem["Energie"];
      setSelectedIngredientsEnergy(energy);
      }
    }
    console.log(totalEnergy)
  };


  const addSearchBar = () => {
    setSearchBars([...searchBars, {}]);
  };

  const handleQuantityChange = (value, index) => {
    const newQuantities = [...selectedIngredientQuantities];
    newQuantities[index] = value;
    setSelectedIngredientQuantities(newQuantities);
  };

  const totalCarbon = searchBars.reduce((acc, _, index) => {
    const carbonPerPortion = selectedIngredients[index] && selectedIngredientQuantities[index] ? 
      (selectedIngredients[index].Total * selectedIngredientQuantities[index]) / numberOfPortions : 0;
    return acc + carbonPerPortion;
  }, 0);

  const totalEnergy = searchBars.reduce((acc, _, index) => {
    const energyPerPortion = selectedIngredients[index] && selectedIngredientsEnergy[index] ? 
      (selectedIngredientQuantities[index] * selectedIngredientsEnergy[index]) / numberOfPortions / 100 : 0;
    return acc + energyPerPortion;
  }, 0); 

  
  return (
    <div className="App">
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <button onClick={exportToExcel} >Exporter au format xlsx</button>
      </div>
      <input
        type="file"
        ref={fileInputRef}
        onChange={handleImportExcel}
        accept=".xlsx, .xls"
        style={{ display: 'none' }} // Masque l'élément input
      />
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <button onClick={handleImportClick}>Importer un fichier</button>
      </div>

      <TextField 
        label="Nom de la recette" 
        value={recipeName} 
        onChange={(e) => setRecipeName(e.target.value)} 
        false
      />
      <TextField 
        label="Nombre de portions" 
        value={numberOfPortions} 
        onChange={(e) => setNumberOfPortions(e.target.value)} 
        true
      />

      <h1 style={{ textAlign: 'left' }}>Empreinte carbone de la recette : {totalCarbon.toFixed(0)} gCO2 par portion </h1>
      
      <h1 style={{ textAlign: 'left' }}>Energie : {totalEnergy.toFixed(0)} kcal par portion </h1>
      

      <h1 style={{ textAlign: 'left' }}>Ingrédients :</h1>
      <table style={{ borderCollapse: 'collapse' }}>
        <thead>
          <tr style={{ border: '1px solid black' }}>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Nom</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Empreinte carbone de l'aliment <br /> (gCO2/g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Quantité <br /> (g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Quantité/portion <br /> (g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Empreinte carbone par portion <br /> (gCO2)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Pourcentage de <br /> l'empreinte carbone</th> 
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Energie<br /> (kcal/100g)</th>
            <th style={{ border: '1px solid black', padding: '2px 10px' }}>Energie<br /> (kcal/portion)</th> 
          </tr>
        </thead>
        <tbody>
          {searchBars.map((_, index) => {
            const carbonPerPortion = selectedIngredients[index] && selectedIngredientQuantities[index] ? 
              ((selectedIngredients[index].Total * selectedIngredientQuantities[index]) / numberOfPortions) : 0;

            const totalCarbon = searchBars.reduce((acc, _, i) => {
              return acc + (selectedIngredients[i]?.Total * selectedIngredientQuantities[i] || 0) / numberOfPortions;
            }, 0);

            const percentage = totalCarbon ? ((carbonPerPortion / totalCarbon) * 100).toFixed(0) : '---';

  

            const energyTotal = selectedIngredientsEnergy[index] * selectedIngredientQuantities[index] / 100 / numberOfPortions || 0;
            
            return (
              <tr key={index}>
                <td>
                  <IngredientsArea
                    index={index}
                    ingredients={ingredients}
                    searchQuery={searchQuery}
                    handleSearchQuery={handleSearchQuery}
                    handleSelection={(ingredients) => handleSelection(searchQuery, index)}
                    filteredIngredients={filteredIngredients[index] || []}
                  />
                </td>
                <td>
                  {selectedIngredients[index]?.Total || '---'}
                </td>
                <td>
                  <input 
                    type="number" 
                    value={selectedIngredientQuantities[index] || ''}
                    onChange={(e) => handleQuantityChange(e.target.value, index)} 
                  />
                </td>
                <td>
                  {selectedIngredientQuantities[index] ? (selectedIngredientQuantities[index] / numberOfPortions).toFixed(1) : '---'}
                </td>
                <td>
                  {carbonPerPortion.toFixed(0) || '---'}
                </td>
                <td>
                  {percentage + ' %'}
                </td>
                <td>
                  {selectedIngredientsEnergy[index]}
                </td>
                <td>
                  {energyTotal}
                </td>
              </tr>
            );
          })}
        </tbody>
        <tfoot>
          <tr>
            <td>
              <button onClick={addSearchBar}>Ajouter ingrédient</button>
            </td>
            <td></td>
          </tr>
        </tfoot>
      </table>

      <h1 style={{ textAlign: 'left' }}>Étapes de la recette</h1>
        <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start" }}>
          {steps.map((step, index) => (
            <textarea
              key={index}
              value={step}
              onChange={(e) => handleStepChange(e, index)}
              style={{ width: '90%' }}
            />
          ))}
          <button onClick={addStep} style={{ alignSelf: "flex-start" }}>Ajouter Étape</button>
        </div>


    </div>
  );
}

export default App;
