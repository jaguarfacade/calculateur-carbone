import React, { useState, useEffect } from 'react';
import { removeAccents } from './Utils' 

const IngredientsArea = ({ ingredients, searchQuery, handleSearchQuery, handleSelection, index, filteredIngredients }) => {

  

  return (
    <div>
      <input 
        list={"ingredients-" + index} 
        value={searchQuery[index] || ''}
        onChange={(event) => handleSearchQuery(event.target.value, index)} 
        onBlur={() => handleSelection(searchQuery, index)} 
      />
      <datalist id={"ingredients-" + index}>
        {filteredIngredients.map((ingredient, i) => (
          <option key={ingredient['Nom du Produit en Français'] + i} value={(ingredient['Nom du Produit en Français'])}>
            {(ingredient['Nom du Produit en Français'])}
          </option>
        ))}
      </datalist>


    </div>
  );
};

export default IngredientsArea;