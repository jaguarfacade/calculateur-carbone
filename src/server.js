const fs = require('fs');

// Lecture de la base de données Agribalyse et conversion en object JS
let rawData = fs.readFileSync('C:\\Users\\Arthur\\Documents\\Cuisine et Climat\\Recettes - Calculateur carbone\\calculateur_carbone_2\\server\\Agribalyse_base_donnees.json');
let ingredients_database = JSON.parse(rawData);

export default ingredients_database;