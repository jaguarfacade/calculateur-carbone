import React, { useState } from 'react';
import './TextField.css';

const TextField = ({ label, value, onChange, isNumber }) => {
  const [errorMessage, setErrorMessage] = useState(null);

  const handleChange = (e) => {
    const val = e.target.value;
    if (!isNumber || (isNumber && val === '' || (!isNaN(val) && Number(val) > 0))) {
      setErrorMessage(null);
      onChange(e);
    } else {
      setErrorMessage('Doit être un nombre supérieur à 0');
    }
  };

  return (
    <div className="textField">
      <h1 className="label">{label} :</h1>
      <input
        type="text"
        value={value}
        onChange={handleChange}
        className={`inputField`}
      />
      {errorMessage && <span style={{ color: 'red', marginLeft: '10px' }}>{errorMessage}</span>}
    </div>
  );
};

export default TextField;
