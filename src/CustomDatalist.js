// CustomDatalist.js
import React, { useState } from 'react';

const CustomDatalist = ({ filteredIngredients, searchQuery }) => {
  const [showSuggestions, setShowSuggestions] = useState(false);

  const handleInputChange = (e) => {
    setShowSuggestions(true);
  };

  return (
    <div>
      <input value={searchQuery} onChange={handleInputChange} />
      {showSuggestions && (
        <div>
          {filteredIngredients.map((ingredient, i) => (
            <div key={i}>
              {ingredient['Nom du Produit en Français']}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default CustomDatalist;
