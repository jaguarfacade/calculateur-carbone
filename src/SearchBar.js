import React from 'react';

function SearchBar({ ingredients }) {
    return (
      <div className="search-bar">
        <input className="search-input" type="text" placeholder="Rechercher un ingrédient" />
        <select className="search-select">
          {ingredients.slice(0, 10).map((ingredient, index) => (
            <option key={index} value={ingredient[0]}>
              {ingredient[0]}
            </option>
          ))}
        </select>
      </div>
    );
  }
export default SearchBar;  
